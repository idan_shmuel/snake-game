var SIDE = 15;
var snake = [{x:6,y:7}, {x:5,y:7}, {x:4,y:7}];
var TILES_PER_SECOND = 10;
var lose = false;
var direction = "stand";
var lastMove = "stand";
var apple;
var lastUpdateTime = 0;
var score = 0;

function game(currTime) {
    if (!lose) {
        window.requestAnimationFrame(game);
    } else {
        alert('GAME OVER! Your score is ' + score);
        return;
    }
    if ((currTime - lastUpdateTime) / 1000 < 1 / TILES_PER_SECOND) {
        return;
    }
    update();
    lastUpdateTime = currTime;
}

document.addEventListener("keydown", e =>{
    if ((direction == "stand" && e.key == "ArrowLeft") || e.key != "ArrowLeft" && e.key != "ArrowRight" 
    && e.key != "ArrowUp" && e.key != "ArrowDown") {
        return;
    }
    if((lastMove == "ArrowUp" && e.key == "ArrowDown") || (lastMove == "ArrowDown" && e.key == "ArrowUp") 
    || (lastMove == "ArrowRight" && e.key == "ArrowLeft") || (lastMove == "ArrowLeft" && e.key == "ArrowRight")) {
        return;
    }
    direction = e.key;
})

function drawApple(apple) {
    if(snake.length > 0) {
        document.getElementsByClassName("box")[apple.y*15 + apple.x].classList.add("apple");
    }
}

function generateApples() {
    apple = {x:Math.floor(Math.random() * 14), y:Math.floor(Math.random() * 14)};
    for(let i = 0;i < snake.length;i++) {
        while(snake[i].x == apple.x && snake[i].y == apple.y) {
            apple = {x:Math.floor(Math.random() * 14), y:Math.floor(Math.random() * 14)};
            i = 0;
        }
    }
    drawApple(apple);
}

function update() {
    moveSnake();
}

function drawSnake() {
    Array.from(document.getElementsByClassName("snake")).forEach(element => {
        element.classList.remove("snake");
    });

    Array.from(document.getElementsByClassName("head")).forEach(element => {
        element.classList.remove("head");
    });

    for(let i = 0;i < snake.length;i++) {
        let snakeBox = document.getElementsByClassName("box")[snake[i].y * SIDE + snake[i].x];
        snakeBox.classList.add("snake");
        if (i == 0) {
            snakeBox.classList.add("head");
        }
    }
    
    if(apple != undefined && snake[0].x == apple.x && snake[0].y == apple.y) {
        document.getElementsByClassName("box")[snake[0].y * SIDE + snake[0].x].classList.remove("apple");
        snake.push({x: snake[snake.length - 1].x, y: snake[snake.length - 1].y})
        score++;
        generateApples();
    }

}

function collision() {
    for (let index = 1; index < snake.length; index++) {
        const element = snake[index];
        if (element.x == snake[0].x && element.y == snake[0].y) {
            return true;
        }
    }
    return false;
}

function moveSnake() {
    if (direction == "stand") {
        return;
    }
    for(let i = snake.length - 1;i > 0;i--) {
        snake[i].x = snake[i-1].x;
        snake[i].y = snake[i-1].y;
    }
    switch (direction) {
        case "ArrowUp":
            snake[0].y -= 1;
            lastMove = "ArrowUp";
            break;

        case "ArrowDown":
            snake[0].y += 1;
            lastMove = "ArrowDown";
            break;

        case "ArrowRight":
            snake[0].x += 1;
            lastMove = "ArrowRight";
            break;

        case "ArrowLeft":
            snake[0].x -= 1;
            lastMove = "ArrowLeft";
            break;
        default:
            break;
    }
    if (snake[0].x < 0 || snake[0].x >= SIDE || snake[0].y < 0 || snake[0].y >= SIDE || collision()) {
        lose = true;
    } else {
        drawSnake();
    }
}

function drawBoard() {
    Object.assign(board.style,{
        'margin-top': '1.5%',
        'margin-bottom': '1.5%',
        'grid-template-rows': 'repeat('+ SIDE +', 1fr)',
        'grid-template-columns': 'repeat('+ SIDE +', 1fr)',
        'height': '90vmin',
        'width': '90vmin',
    });
    for (let index = 0; index < SIDE ** 2; index++) {
        let box = document.createElement('div');
        box.classList.add('box');
        if (index % 2 != 0) {
            box.classList.add('dark');
        }
        board.appendChild(box);
    }
}

$(document).ready(function () {
    drawBoard();
    drawSnake();
    generateApples();
    window.requestAnimationFrame(game);
});